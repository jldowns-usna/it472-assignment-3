package edu.usna.mobileos.assignment3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import java.util.*

class AddItemActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)

        findViewById<Button>(R.id.addSaveButton).setOnClickListener(this)
        findViewById<Button>(R.id.addDiscardButton).setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val resultIntent = Intent()


        when(v!!.id) {
            R.id.addSaveButton -> {
                val title = findViewById<EditText>(R.id.addItemTitleField).text.toString()
                val description = findViewById<EditText>(R.id.addItemDescriptionField).text.toString()
                val now = Calendar.getInstance()
                val newTodoItem = ToDoItem(title, description, now)
                resultIntent.putExtra("newTodoItem", newTodoItem)
                setResult(RESULT_OK, resultIntent)
                finish()
            }
            R.id.addDiscardButton -> {
                // discard
            }
        }

        setResult(RESULT_CANCELED, resultIntent)
        finish()

    }

}