package edu.usna.mobileos.assignment3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.EditText

class EditItem : AppCompatActivity(), OnClickListener {
    lateinit var todoItem: ToDoItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_item)

        todoItem = intent.getSerializableExtra("todoItem") as ToDoItem
        findViewById<EditText>(R.id.editItemTitleField).setText(todoItem.title)
        findViewById<EditText>(R.id.editItemDescriptionField).setText(todoItem.description)

        findViewById<Button>(R.id.editSaveButton).setOnClickListener(this)
        findViewById<Button>(R.id.editDiscardButton).setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.editSaveButton -> {
                todoItem.title = findViewById<EditText>(R.id.editItemTitleField).text.toString()
                todoItem.description = findViewById<EditText>(R.id.editItemDescriptionField).text.toString()
            }
            R.id.editDiscardButton -> {
                // discard
            }
        }

        val resultIntent = Intent()

        resultIntent.putExtra("updatedTodoItem", todoItem)
        setResult(RESULT_OK, resultIntent)
        finish()


    }

}