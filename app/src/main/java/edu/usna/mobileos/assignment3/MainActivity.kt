package edu.usna.mobileos.assignment3

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import java.io.IOException
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.time.format.DateTimeFormatter
import java.util.*

class MainActivity : AppCompatActivity(), RecyclerClickHandler {
    var editItemRequestCode = 2234
    var addItemRequestCode = 2233
    val saveFileName: String = "todoArray"
    lateinit var todoArray: Array<ToDoItem>
    lateinit var todoAdapter: TodoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // try loading todo list from file
        val tryTodoArray: Any? = getObjectFromFile(saveFileName)
        if (tryTodoArray != null) {
            todoArray = tryTodoArray as Array<ToDoItem>
            Snackbar.make(findViewById(android.R.id.content), "Loaded items from file.", Snackbar.LENGTH_LONG).show()
        } else {
            val now = Calendar.getInstance()
            todoArray = arrayOf(
                ToDoItem("Buy Milk", "I need to buy milk.", now),
                ToDoItem("Wash Car", "My car is dirty. I need to clean it.", now),
                ToDoItem("Make doc appt", "Phone number: 561-567-9383", now),
            )
            Snackbar.make(findViewById(android.R.id.content), "No previous file found. Using defaults.", Snackbar.LENGTH_LONG).show()

        }


        // set up recycler view
        todoAdapter = TodoAdapter(todoArray, this)
        findViewById<RecyclerView>(R.id.todo_recycler).adapter = todoAdapter

    }

    // Called when a todo item is clicked.
    override fun onItemClick(todoItem: ToDoItem) {
        Snackbar.make(findViewById(android.R.id.content), todoItem.title, Snackbar.LENGTH_LONG).show()
    }

    // Called when a context menu is clicked
    override fun onContextItemSelected(menuItem: MenuItem): Boolean {

        //get the position of the selected item
        //menuItem.groupId can be used since that is what we set-up
        //in the context menu in the ViewHolder
        val position = menuItem.groupId
        val contextMenuItem: Int = menuItem.itemId
        val todoItem = todoArray[position]


        return when( menuItem.itemId) {
            R.id.editDetailContextItem -> {
                editDetail(todoItem)
                true
            }
            R.id.showDetailContextItem -> {
                showDetail(todoItem)
                true
            }
            R.id.deleteContextItem -> {
                deleteItem(todoItem)
                true
            }
            else -> super.onContextItemSelected(menuItem)
        }


    }

    fun showDetail(todoItem: ToDoItem) {
        val showDetailDialog = ShowDetailsDialog(todoItem)
        showDetailDialog.show(supportFragmentManager, "ShowDetailsDialog")
    }

    fun editDetail(todoItem: ToDoItem) {
        val resultIntent = Intent(this, EditItem::class.java)
        resultIntent.putExtra("todoItem", todoItem)
        startActivityForResult(resultIntent, editItemRequestCode)

    }

    fun deleteItem(todoItem: ToDoItem) {
        val deleteItemDialog = DeleteItemDialog(todoItem) {todoItem: ToDoItem ->
            todoArray = todoArray.filter { x-> x.id != todoItem.id}.toTypedArray() // Ok, this is a little gross.
            Log.i("IT472", "New array: ${todoArray.map{x->x.title}.joinToString(", ")}")
            updateTodoArray(todoArray)

        }
        deleteItemDialog.show(supportFragmentManager, "DeleteItemDialog")
    }

    // call this every time you update the todoArray. It will updated the
    // recyclerview and also save to file.
    fun updateTodoArray(updatedTodoArray: Array<ToDoItem>) {
        todoArray = updatedTodoArray
        todoAdapter.update(updatedTodoArray, "ok")
        saveObjectToFile(saveFileName, updatedTodoArray)
    }

    //
    // Handle Returning Activities
    //
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        // EditItem Activity
        if (requestCode == this.editItemRequestCode) {
            // check result code
            if (resultCode == RESULT_OK) {
                val updatedTodoItem = data?.getSerializableExtra("updatedTodoItem") as ToDoItem
                if (updatedTodoItem != null) {
                    // Do the edit here
                    var todoHandle = todoArray.find { it.id == updatedTodoItem.id }
                    todoHandle!!.title = updatedTodoItem.title
                    todoHandle!!.description = updatedTodoItem.description
                    updateTodoArray(todoArray)
                }
            }
        }

        // AddItem Activity
        if (requestCode == this.addItemRequestCode) {
            // check result code
            if (resultCode == RESULT_OK) {
                val newTodoItem = data?.getSerializableExtra("newTodoItem") as ToDoItem
                if (newTodoItem != null) {
                    // Do the edit here
                    todoArray += newTodoItem
                    updateTodoArray(todoArray)
                }
            }
        }



    }

    // Create Options Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.optionsMenuAdd -> {
                val resultIntent = Intent(this, AddItemActivity::class.java)
                startActivityForResult(resultIntent, addItemRequestCode)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun saveObjectToFile(fileName: String, obj: Any) {
        try {
            ObjectOutputStream(openFileOutput(fileName, MODE_PRIVATE)).use {
                it.writeObject(obj)
            }
        }
        catch (e: IOException){
            Log.e("IT472", "IOException writing file $fileName")
        }
    }

    fun getObjectFromFile(fileName: String): Any? {
        try{
            ObjectInputStream(openFileInput(fileName)).use{
                return it.readObject()
            }
        }
        catch (e: IOException){
            Log.e("IT472", "IOException reading file $fileName")
            return null
        }
    }
}


data class ToDoItem(var title: String, var description: String = "", val dateCreated: Calendar): Serializable {
    val id: String = getRandomString(10)

    fun getRandomString(length: Int) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }
}


// This creates the dialog menu for the Show Details Page.
class ShowDetailsDialog(val todoItem: ToDoItem) : DialogFragment(), DialogInterface.OnClickListener {

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        val inflater = LayoutInflater.from(activity)
        val layout = inflater.inflate(R.layout.show_details_dialog, null)
        builder.setView(layout) //set the view in dialog to custom layout

        // populate all fields
        layout.findViewById<TextView>(R.id.descriptionField).setText(todoItem.description)
        val dateFormat = SimpleDateFormat("E, MMM d 'at' H:m")
        layout.findViewById<TextView>(R.id.creationDateField).setText("Created: ${dateFormat.format(todoItem.dateCreated.time)}")

        // set up dialog-specific items like buttons and title
        builder.setTitle(todoItem.title)
            .setPositiveButton("OK", this)
            .setNegativeButton("Cancel", this)


        return builder.create()
    }

    override fun onClick(dialog: DialogInterface, itemId: Int) {
        if (itemId == Dialog.BUTTON_POSITIVE) {
            Log.d("IT472", "Think positive!")
        } else {
            Log.d("IT472", "You cancelled your input")
        }
    }
}

class DeleteItemDialog(val todoItem: ToDoItem, val deleteFunction: (ToDoItem) -> Unit): DialogFragment(), DialogInterface.OnClickListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(activity)

        builder.setTitle("Deleting Item")
        builder.setMessage("Are you sure you want to delete ${todoItem.title}?")
        builder.setPositiveButton("Yes", this)
        builder.setNegativeButton("No", this)

        return builder.create()
    }

    override fun onClick(dialog: DialogInterface, itemId: Int) {
        if (itemId == Dialog.BUTTON_POSITIVE) {
            Log.d("IT472", "Think positive!")
            deleteFunction(todoItem)
        } else {
            Log.d("IT472", "You cancelled your input")
        }
    }

}