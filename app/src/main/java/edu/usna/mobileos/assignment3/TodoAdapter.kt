package edu.usna.mobileos.assignment3

import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

interface RecyclerClickHandler {
    fun onItemClick(todoItem: ToDoItem)
}

class TextItemViewHolder(view: View): RecyclerView.ViewHolder(view), View.OnCreateContextMenuListener{
    val textView: TextView = view.findViewById(R.id.itemTextView)

    fun onBind(todoItem: ToDoItem, clickListener: RecyclerClickHandler) {
        textView.text = todoItem.title // set text/appearance
        textView.setOnClickListener{clickListener.onItemClick(todoItem)} // set click
        textView.setOnCreateContextMenuListener(this) // Set context menu
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        // adapterPosition will be the index of the todo item.
        menu?.add(adapterPosition, R.id.showDetailContextItem, 1, "Show Detailz")
        menu?.add(adapterPosition, R.id.editDetailContextItem, 2, "Edit Detailz")
        menu?.add(adapterPosition, R.id.deleteContextItem, 3, "Delete Itemz")
    }
}

class TodoAdapter(var data: Array<ToDoItem>, val clickHandler: RecyclerClickHandler) : RecyclerView.Adapter<TextItemViewHolder>() {
    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater
            .inflate(R.layout.todo_item, parent, false)
        return TextItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: TextItemViewHolder, position: Int) {
        holder.onBind(data[position], clickHandler)
    }

    // From: https://stackoverflow.com/questions/53005135/how-to-update-my-recyclerview-using-kotlin-android
    fun update(newData: Array<ToDoItem>, check: String) {
        data = newData
        this.notifyDataSetChanged()
    }

}